import React from 'react';

import logo from '../../assets/logo/logo.svg';
import './Header.css';

function Header(props) {
  const { handleToggleForm, isLoggedOrRegistered, handleLogOut } = props;

  return (
    <header>
      <img src={logo} alt="Logo Full Stack Charts" />
      <div>
        {isLoggedOrRegistered ? (
          <button onClick={handleLogOut}>Sign Out</button>
        ) : (
          <div>
            <button onClick={() => handleToggleForm('signIn')}>Sign In</button>
            <button onClick={() => handleToggleForm('register')}>
              Register
            </button>
          </div>
        )}
      </div>
    </header>
  );
}

export default Header;
