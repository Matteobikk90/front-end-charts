import React from 'react';
import { Chart } from 'react-google-charts';

import loader from '../../assets/logo/loader.svg';
import plus from '../../assets/logo/plus.svg';

import './Charts.css';

function Charts(props) {
  const {
    handleActionsChart,
    handleSelectOnChangeChartType,
    handleSubmitChart,
    handleUpdateChart,
    charts,
  } = props;

  console.log(charts);
  let mapCharts;

  if (charts.length !== 0) {
    mapCharts = charts.map((chart) => {
      let options = {
        animation: {
          startup: true,
          easing: 'in',
          duration: 1000,
        },
        title: chart.title,
        chartArea: { width: '50%', height: '80%' },
      };

      if (chart.type === 'is3D') {
        options = {
          ...options,
          is3D: true,
        };
      } else if (chart.type === 'pieHole') {
        options = {
          ...options,
          pieHole: 0.4,
        };
      } else if (chart.type === 'slices') {
        options = {
          ...options,
          legend: 'none',
          pieSliceText: 'label',
          slices: {
            1: { offset: 0.2 },
            2: { offset: 0.3 },
            3: { offset: 0.4 },
            4: { offset: 0.5 },
          },
        };
      } else {
        options = {
          ...options,
        };
      }

      return (
        <article key={chart.chart_id}>
          <div className="inner">
            <div className="front">
              <Chart
                width={'100%'}
                height={'300px'}
                chartType="PieChart"
                loader={<img src={loader} alt="Loader img" />}
                data={[
                  [chart.data1, chart.data2],
                  [chart.data3, chart.data4],
                  [chart.data5, chart.data6],
                  [chart.data7, chart.data8],
                  [chart.data9, chart.data10],
                ]}
                options={options}
                rootProps={{ 'data-testid': chart.chart_id }}
              />
              <button
                className="updateBtn"
                onClick={(e) => handleActionsChart(e, 'update')}
                id={chart.chart_id}
              >
                Update
              </button>
              <button
                className="deleteBtn"
                onClick={(e) => handleActionsChart(e, 'delete')}
                id={chart.chart_id}
              >
                Delete
              </button>
            </div>
            <form
              className="back"
              onSubmit={(e) => handleUpdateChart(e, chart.chart_id)}
            >
              <div>
                <input
                  type="text"
                  name="title"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.title}
                />
                <label>
                  Types:
                  <select onChange={handleSelectOnChangeChartType} name="type">
                    <option value="PieChart">Pie Chart</option>
                    <option value="is3D">3D Pie Chart</option>
                    <option value="pieHole">Donut Chart</option>
                    <option value="slices">Exploding Chart</option>
                  </select>
                </label>
              </div>
              <div>
                <h5>Categories</h5>
                <h5>Volume</h5>
              </div>
              <h5>Data</h5>
              <div>
                <h5>String</h5>
                <h5>Integer</h5>
              </div>
              <div>
                <input
                  type="text"
                  name="data1"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data1}
                />
                <input
                  type="text"
                  name="data2"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data2}
                />
              </div>
              <div>
                <input
                  type="text"
                  name="data3"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data3}
                />
                <input
                  type="text"
                  name="data4"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data4}
                />
              </div>
              <div>
                <input
                  type="text"
                  name="data5"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data5}
                />
                <input
                  type="text"
                  name="data6"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data6}
                />
              </div>
              <div>
                <input
                  type="text"
                  name="data7"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data7}
                />
                <input
                  type="text"
                  name="data8"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data8}
                />
              </div>
              <div>
                <input
                  type="text"
                  name="data9"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data9}
                />
                <input
                  type="text"
                  name="data10"
                  onChange={handleSelectOnChangeChartType}
                  placeholder={chart.data10}
                />
              </div>
              <button className="updateBtn" type="submit">
                Update
              </button>
            </form>
          </div>
        </article>
      );
    });
  }

  return (
    <section className="chartsGrid">
      <h2>
        {/* Welcome {charts[0].name}, you have {charts.data.length} entries */}
      </h2>
      {mapCharts}
      <article onClick={(e) => handleActionsChart(e, 'add')}>
        <div className="inner">
          <div className="front">
            <img src={plus} alt="Plus icon" />
          </div>
          <form className="back" onSubmit={handleSubmitChart}>
            <div>
              <input
                type="text"
                name="title"
                onChange={handleSelectOnChangeChartType}
                placeholder="Name"
              />
              <label>
                Types:
                <select onChange={handleSelectOnChangeChartType} name="type">
                  <option value="PieChart">Pie Chart</option>
                  <option value="is3D">3D Pie Chart</option>
                  <option value="pieHole">Donut Chart</option>
                  <option value="slices">Exploding Chart</option>
                </select>
              </label>
            </div>
            <div>
              <h5>Categories</h5>
              <h5>Volume</h5>
            </div>
            <h5>Data</h5>
            <div>
              <h5>String</h5>
              <h5>Integer</h5>
            </div>
            <div>
              <input
                type="text"
                name="data1"
                onChange={handleSelectOnChangeChartType}
                placeholder="Ex: Work"
              />
              <input
                type="text"
                name="data2"
                onChange={handleSelectOnChangeChartType}
                placeholder="Ex: 8"
              />
            </div>
            <div>
              <input
                type="text"
                name="data3"
                onChange={handleSelectOnChangeChartType}
                placeholder="Ex: Sleep"
              />
              <input
                type="text"
                name="data4"
                onChange={handleSelectOnChangeChartType}
                placeholder="Ex: 8"
              />
            </div>
            <div>
              <input
                type="text"
                name="data5"
                onChange={handleSelectOnChangeChartType}
              />
              <input
                type="text"
                name="data6"
                onChange={handleSelectOnChangeChartType}
              />
            </div>
            <div>
              <input
                type="text"
                name="data7"
                onChange={handleSelectOnChangeChartType}
              />
              <input
                type="text"
                name="data8"
                onChange={handleSelectOnChangeChartType}
              />
            </div>
            <div>
              <input
                type="text"
                name="data9"
                onChange={handleSelectOnChangeChartType}
              />
              <input
                type="text"
                name="data10"
                onChange={handleSelectOnChangeChartType}
              />
            </div>
            <button className="addBtn" type="submit">
              Add
            </button>
          </form>
        </div>
      </article>
    </section>
  );
}

export default Charts;
