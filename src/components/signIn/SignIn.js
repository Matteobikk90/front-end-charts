import React from 'react';

import './SignIn.css';

function SignIn(props) {
  const { userSignInDetails, handleChangeSignIn, handleSubmitSignIn } = props;

  return (
    <form className="signIn" onSubmit={handleSubmitSignIn}>
      <label htmlFor="emailInput">Email</label>
      <input
        required
        id="emailInput"
        type="email"
        name="email"
        value={userSignInDetails.email}
        onChange={handleChangeSignIn}
      />
      <label htmlFor="passwordInput">Password</label>
      <input
        required
        id="passwordInput"
        type="password"
        name="password"
        value={userSignInDetails.password}
        onChange={handleChangeSignIn}
      />
      <input type="submit" value="Sign In" />
    </form>
  );
}

export default SignIn;
