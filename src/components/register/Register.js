import React from 'react';

import './Register.css';

function Register(props) {
  const {
    userRegisterDetails,
    handleChangeRegister,
    handleSubmitRegister,
  } = props;

  return (
    <form className="register" onSubmit={handleSubmitRegister}>
      <label htmlFor="nameInput">Name</label>
      <input
        required
        id="nameInput"
        type="text"
        name="name"
        value={userRegisterDetails.name}
        onChange={handleChangeRegister}
      />
      <label htmlFor="emailInputRegister">Email</label>
      <input
        required
        id="emailInputRegister"
        type="email"
        name="email"
        value={userRegisterDetails.email}
        onChange={handleChangeRegister}
      />
      <label htmlFor="passwordRegisterInput">Password</label>
      <input
        required
        id="passwordRegisterInput"
        type="password"
        name="userPassword"
        value={userRegisterDetails.userPassword}
        onChange={handleChangeRegister}
      />
      <input type="submit" value="Register" />
    </form>
  );
}

export default Register;
