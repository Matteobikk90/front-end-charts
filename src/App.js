import React, { useState, useEffect, Fragment } from 'react';
import Particles from 'react-particles-js';
import Header from './components/header/Header';
import SignIn from './components/signIn/SignIn';
import Register from './components/register/Register';
import Charts from './components/charts/Charts';

import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import './App.css';

function App() {
  const [userSignInDetails, setUserSignInDetails] = useState({
    email: '',
    password: '',
  });

  const [userRegisterDetails, setUserRegisterDetails] = useState({
    email: '',
    userPassword: '',
    name: '',
  });

  const [toggleForms, setToggleForms] = useState({
    signIn: true,
    register: false,
  });

  const [charts, setCharts] = useState([]);

  const [chart, setChart] = useState({
    userid: null,
    title: null,
    type: 'PieChart',
    data1: null,
    data2: null,
    data3: null,
    data4: null,
    data5: null,
    data6: null,
    data7: null,
    data8: null,
    data9: null,
    data10: null,
  });

  useEffect(() => {
    if (chart.userid) {
      fetch(
        `https://full-stack-charts-api.herokuapp.com/getCharts/${chart.userid}`
      )
        .then((response) => response.json())
        .then((data) => setCharts(...charts, data))
        .catch((err) => console.log(err));
    }
  }, [chart.userid]);

  const handleSubmitChart = (e) => {
    e.preventDefault();
    e.currentTarget.parentElement.parentElement.classList.remove('active');
    fetch('https://full-stack-charts-api.herokuapp.com/addChart', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        userid: chart.userid,
        title: chart.title,
        type: chart.type,
        data1: chart.data1,
        data2: chart.data2,
        data3: chart.data3,
        data4: chart.data4,
        data5: chart.data5,
        data6: chart.data6,
        data7: chart.data7,
        data8: chart.data8,
        data9: chart.data9,
        data10: chart.data10,
      }),
    })
      .then((response) => response.json())
      .then((chart) => {
        setCharts([...charts, chart]);
        toast.success('Chart successfully created', {
          position: 'bottom-left',
          pauseOnHover: false,
          draggable: false,
        });
      })
      .catch((err) => {
        console.log(err);
        toast.error('Chart not added, try again', {
          position: 'bottom-left',
          pauseOnHover: false,
          draggable: false,
        });
      });
  };

  const handleUpdateChart = (e, id) => {
    e.preventDefault();
    e.currentTarget.parentElement.parentElement.classList.remove('active');
    fetch(`https://full-stack-charts-api.herokuapp.com/updateChart/${id}`, {
      method: 'put',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        userid: chart.userid,
        title: chart.title,
        type: chart.type,
        data1: chart.data1,
        data2: chart.data2,
        data3: chart.data3,
        data4: chart.data4,
        data5: chart.data5,
        data6: chart.data6,
        data7: chart.data7,
        data8: chart.data8,
        data9: chart.data9,
        data10: chart.data10,
      }),
    })
      .then((response) => response.json())
      .then((chart) => {
        const elementIndex = charts.findIndex(
          (x) => x.chart_id === chart.chart_id
        );
        let newArray = [...charts];
        newArray[elementIndex] = { ...newArray[elementIndex], ...chart };
        setCharts(newArray);
        toast.success('Chart successfully updated', {
          position: 'bottom-left',
          pauseOnHover: false,
          draggable: false,
        });
      })
      .catch((err) => {
        console.log(err);
        toast.error('Chart not updated, try again', {
          position: 'bottom-left',
          pauseOnHover: false,
          draggable: false,
        });
      });
  };

  const handleSelectOnChangeChartType = (e) => {
    const value = e.target.value;
    setChart({
      ...chart,
      [e.target.name]: value,
    });
  };

  const [isLoggedOrRegistered, setIsLoggedOrRegistered] = useState(false);

  const handleLogOut = () => {
    setIsLoggedOrRegistered(false);
    toast.info('Successfully logged out', {
      position: 'bottom-left',
      pauseOnHover: false,
      draggable: false,
    });
  };

  const handleToggleForm = (name) => {
    setToggleForms({ [name]: !toggleForms.name });
  };

  const handleChangeSignIn = (e) => {
    const value = e.target.value;
    setUserSignInDetails({
      ...userSignInDetails,
      [e.target.name]: value,
    });
  };

  const handleChangeRegister = (e) => {
    const value = e.target.value;
    setUserRegisterDetails({
      ...userRegisterDetails,
      [e.target.name]: value,
    });
  };

  const handleSubmitRegister = (e) => {
    e.preventDefault();
    fetch('https://full-stack-charts-api.herokuapp.com/register', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: userRegisterDetails.email,
        password: userRegisterDetails.userPassword,
        name: userRegisterDetails.name,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === 'Unable to register') {
          toast.error('Unable to register, try again', {
            position: 'bottom-left',
            pauseOnHover: false,
            draggable: false,
          });
          setIsLoggedOrRegistered(false);
        } else if (data === 'Email already registered') {
          toast.error('Email already registered, try with a different one', {
            position: 'bottom-left',
            pauseOnHover: false,
            draggable: false,
          });
          setIsLoggedOrRegistered(false);
        } else {
          toast.success('Successfully registered', {
            position: 'bottom-left',
            pauseOnHover: false,
            draggable: false,
          });
          setIsLoggedOrRegistered(true);
          setChart({
            ...chart,
            userid: data.id,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleSubmitSignIn = (e) => {
    e.preventDefault();
    const url = 'https://full-stack-charts-api.herokuapp.com/signin';
    fetch(url, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: userSignInDetails.email,
        password: userSignInDetails.password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === 'wrong credentials') {
          toast.error('Wrong credentials, try again', {
            position: 'bottom-left',
            pauseOnHover: false,
            draggable: false,
          });
          setIsLoggedOrRegistered(false);
        } else if (data === 'unable to get user') {
          toast.warning('Unable to get user, try again', {
            position: 'bottom-left',
            pauseOnHover: false,
            draggable: false,
          });
          setIsLoggedOrRegistered(false);
        } else {
          setChart({
            ...chart,
            userid: data.id,
          });
          setIsLoggedOrRegistered(true);
          toast.success('Successfully logged', {
            position: 'bottom-left',
            pauseOnHover: false,
            draggable: false,
          });
        }
      })
      .catch((err) => console.log(err));
  };

  const handleRemoveChart = (e) => {
    const confirmation = prompt('Type yes to delete this Chart');
    const url = 'https://full-stack-charts-api.herokuapp.com/deleteChart/';
    const { id } = e.currentTarget;
    if (confirmation === 'yes') {
      fetch(url, {
        method: 'delete',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          id: id,
        }),
      })
        .then((res) => res.json())
        .then((chart) => {
          const updatedCharts = [...charts];
          updatedCharts.splice(chart, 1);
          setCharts((charts, updatedCharts));
          toast.warning('Chart successfully removed', {
            position: 'bottom-left',
            pauseOnHover: false,
            draggable: false,
          });
        })
        .catch((err) => console.log(err));
    }
  };

  const handleActionsChart = (e, action) => {
    const selectedArticle =
      e.currentTarget.parentElement.parentElement.parentElement;
    if (action === 'add') {
      e.currentTarget.classList.add('active');
    } else if (action === 'update') {
      selectedArticle.classList.add('active');
    } else {
      handleRemoveChart(e);
    }
  };

  return (
    <Fragment>
      <ToastContainer />
      <Header
        isLoggedOrRegistered={isLoggedOrRegistered}
        handleToggleForm={handleToggleForm}
        handleLogOut={handleLogOut}
      />
      {isLoggedOrRegistered ? (
        <Charts
          handleSubmitChart={handleSubmitChart}
          handleUpdateChart={handleUpdateChart}
          handleSelectOnChangeChartType={handleSelectOnChangeChartType}
          handleActionsChart={handleActionsChart}
          charts={charts}
        />
      ) : (
        <section>
          {toggleForms.signIn ? (
            <SignIn
              userSignInDetails={userSignInDetails}
              handleChangeSignIn={handleChangeSignIn}
              handleSubmitSignIn={handleSubmitSignIn}
              isLoggedOrRegistered={isLoggedOrRegistered}
            />
          ) : null}
          {toggleForms.register ? (
            <Register
              userRegisterDetails={userRegisterDetails}
              handleChangeRegister={handleChangeRegister}
              handleSubmitRegister={handleSubmitRegister}
              isLoggedOrRegistered={isLoggedOrRegistered}
            />
          ) : null}
        </section>
      )}
      <Particles
        className="particles"
        params={{
          particles: {
            number: {
              value: 60,
            },
            line_linked: {
              color: '#ffffff',
            },
            color: {
              value: '#3b3b3b',
            },
            size: {
              value: 1.5,
            },
          },
          interactivity: {
            events: {
              onhover: {
                enable: true,
                mode: 'repulse',
              },
            },
          },
        }}
      />
    </Fragment>
  );
}

export default App;
