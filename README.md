# Assignment

Build a full stack dashboard

A user can login or register at first and then he is able to add, update or delete Charts by using Google Charts library.

The idea was to use Front end, back end and DB technologies by using modern programming languages like React.js, Node.js and PostgreSQL

The client side is uploaded to Firebase Hosting and the server on Heroku

## Approach

I firstly tried to do some desing for the header, dashboard and try to implement some animation through Particles.js

I then started to work on the logic

## Tech/Framework used

- React.js
- Particles.js
- CSS3
- ES6
- Firebase Hosting

## How to run

- clone the repo and run npm start
- or go to the online url - https://full-stack-charts.web.app/

## Issues

- came accross few difficulties when updating the interested Chart
